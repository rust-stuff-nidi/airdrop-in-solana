const {
  Connection,
  PublicKey,
  clusterApiUrl,
  Keypair,
  LAMPORTS_PER_SOL,
} = require("@solana/web3.js");

const wallet = new Keypair();
const publicKey = new PublicKey(wallet._keypair.publicKey);

const getWalletBalance = async () => {
  try {
    const connection = new Connection(clusterApiUrl("devnet"), "confirmed");
    const walletBalance = await connection.getBalance(publicKey);
    return walletBalance;
  } catch (err) {
    console.error(err);
  }
};
const airDropSol = async () => {
  try {
    console.log("Initializing airdrop...");
    const AMOUNT_TO_AIRDROP = 1;
    const connection = new Connection(clusterApiUrl("devnet"), "confirmed");
    const fromAirDropSignature = await connection.requestAirdrop(
      publicKey,
      AMOUNT_TO_AIRDROP * LAMPORTS_PER_SOL
    );
    const latestBlockHash = await connection.getLatestBlockhash();

    await connection.confirmTransaction({
      blockhash: latestBlockHash.blockhash,
      lastValidBlockHeight: latestBlockHash.lastValidBlockHeight,
      signature: fromAirDropSignature,
    });
    console.log("Airdrop finished.");
  } catch (err) {
    console.log(err);
  }
};
const main = async () => {
  const initialBalance = await getWalletBalance();
  console.log(`Initial balance: ${initialBalance}`);

  await airDropSol();

  const updatedBalance = await getWalletBalance();
  console.log(`Updated balance: ${updatedBalance}`);
};

main();
